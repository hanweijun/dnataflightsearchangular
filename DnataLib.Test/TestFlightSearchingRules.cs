﻿using DnataLib.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using DnataLib.Services;
using DnataLib.Entity;

namespace DnataLib.Test
{
    [TestClass]
    public class TestFlightSearchingRules
    {
        [TestMethod]
        public void TestGetDepartbeforeDate()
        {
            var flightSegments = BuildTestData();
            var results = flightSegments.GetDepartbeforeDate(DateTime.Now).ToArray();

            Assert.IsTrue(results.Count()==1 );
        }

        [TestMethod]
        public void TestGetArrivalBeforeDeparture()
        {
            var flightSegments = BuildTestData();
            var results = flightSegments.GetArrivalBeforeDeparture().ToArray();

            Assert.IsTrue(results.Count() == 1);
        }

        private static IQueryable<FlightSegment> BuildTestData()
        {
            var flights = FlightBuilder.GetFlights();
            return ConvertHelper.ConvertToFlightSegmentList(flights).AsQueryable();
        }

    }
}
