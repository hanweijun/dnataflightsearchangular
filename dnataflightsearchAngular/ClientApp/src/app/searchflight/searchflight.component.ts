import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-searchflight',
  templateUrl: './searchflight.component.html'
})
export class searchflightComponent {
  public flights: Flight[] = [];

  rules: Rule[] = [new Rule(1, 'Rule 1'), new Rule(2, 'Rule 2'), new Rule(3, 'Rule 3')];

  public _http: HttpClient;
  public _baseUrl: string;

  constructor(public fb: FormBuilder, http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._http = http;
    this._baseUrl = baseUrl;
  }

  self = this;

  oppoSuitsForm = this.fb.group({
    name: ['', [Validators.required]]
  })

  changeSuit(e:any) {
    this.oppoSuitsForm.setValue(e.target.value, {
      onlySelf: true
    })
  }

  public Search() {
    this._http.get<Flight[]>(this._baseUrl + 'Flight?rule=' + this.oppoSuitsForm.value.name).subscribe(result => {
      this.flights = result;
    }, error => console.error(error));
  }
}

interface Flight {
  flightNo: string;
  segments: Segment[];
}

interface Segment {
  departureDate: string;
  arrivalDate: string;
}

export class Rule {
  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
  id: number;
  name: string;
}
