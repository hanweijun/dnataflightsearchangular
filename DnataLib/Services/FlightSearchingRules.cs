﻿using DnataLib.Entity;

namespace DnataLib.Services
{
    // The searching rules are implemeted using extention methods on IQueryable,
    // so the rules can be chained as late binding.
    public static class FlightSearchingRules
    {
        // Rule1: Depart before the current date/time
        public static IQueryable<FlightSegment> GetDepartbeforeDate(this IQueryable<FlightSegment> flightSegments, DateTime dateTime)
        {
            return flightSegments.Where(s=>s.DepartureDate<dateTime);
        }

        // Rule2: Have any segment with an arrival date before the departure date
        public static IQueryable<FlightSegment> GetArrivalBeforeDeparture(this IQueryable<FlightSegment> flightSegments)
        {
            return flightSegments.Where(s => s.ArrivalDate < s.DepartureDate);
        }

        // Rule3: Spend more than 2 hours on the ground – i.e. those with a total combined gap of over two hours between the arrival date of one segment and the departure date of the next
        // Todo: It's a challenge to implement this rule in sql or EF. For now, I just implement it in ojbect. It's not ideal because it loads more data from database thus poor performance.
    }
}
