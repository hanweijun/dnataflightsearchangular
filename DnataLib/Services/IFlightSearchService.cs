﻿using DnataLib.Model;

namespace DnataLib.Services
{
    public interface IFlightSearchService
    {
        Task<IList<Flight>> GetDepartbeforeCurrentDate();
        Task<IList<Flight>> GetArrivalBeforeDeparture();
        Task<IList<Flight>> GetByRuleOneAndTwo();
        Task<IList<Flight>> GetMoreThan2HourGroud();

        Task InitLocalDB();
    }

}
