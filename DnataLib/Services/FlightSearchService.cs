﻿using DnataLib.Helpers;
using DnataLib.Model;
using DnataLib.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DnataLib.Services
{
    public class FlightSearchService : IFlightSearchService
    {
        private readonly ILogger<FlightSearchService> _log;
        private readonly IConfiguration _config;
        private readonly IMemoryCache _memoryCache;
        private readonly IFlightRepository _flightRepository;
        private readonly int _cacheMinutes;

        public FlightSearchService(ILogger<FlightSearchService> log, IConfiguration config, IMemoryCache memoryCache, IFlightRepository flightRepository)
        {
            _log = log;
            _config = config;
            _memoryCache = memoryCache;
            _flightRepository = flightRepository;
            _cacheMinutes= _config.GetValue<int>("CacheMinutes");
        }


        // Rule1
        // Note: this method is not suitable for caching as 'current datetime' changes for each call
        public async Task<IList<Flight>> GetDepartbeforeCurrentDate()
        {
            _log.LogInformation($"Starting {nameof(GetDepartbeforeCurrentDate)}");

            // Get all flight numbers that match the rule
            var flightNos = await _flightRepository.GetAllFlights()
                .GetDepartbeforeDate(DateTime.Now)
                .Select(s => s.FlightNo).ToArrayAsync();

            if (!flightNos.Any())
                return new List<Flight>();

            // Get all segments for above flight numbers
            var flightSegments = await _flightRepository.GetFlightsByFlightNosAsync(flightNos);

            // convert FlightSegment[] to Flight[]
            return ConvertHelper.ConvertToFlightList(flightSegments.ToList());
        }

        // Rule2
        public async Task<IList<Flight>> GetArrivalBeforeDeparture()
        {
            _log.LogInformation($"Starting {nameof(GetArrivalBeforeDeparture)}");

            // Get from cache first
            var cacheKey = nameof(GetArrivalBeforeDeparture);
            if (_memoryCache.TryGetValue(cacheKey, out IList<Flight> cacheValue))
                return cacheValue;

            // Get all flight numbers that match the rule
            var flightNos = await _flightRepository.GetAllFlights()
            .GetArrivalBeforeDeparture()
            .Select(s => s.FlightNo).ToArrayAsync();

            IList<Flight> flights;
            if (!flightNos.Any())
            {
                flights= new List<Flight>();
            }
            else
            {
                // Get all segments for above flight numbers
                var flightSegments = await _flightRepository.GetFlightsByFlightNosAsync(flightNos);

                // convert FlightSegment[] to Flight[]
                flights = ConvertHelper.ConvertToFlightList(flightSegments.ToList());
            }

            // set cache
            _memoryCache.Set(cacheKey, flights, TimeSpan.FromMinutes(_cacheMinutes));
            return flights;
        }

        // combine Rule1 and Rule2
        // Note: Rule1 and Rule2 are chained as late binding. We may add more rules into the chain.
        public async Task<IList<Flight>> GetByRuleOneAndTwo()
        {
            _log.LogInformation($"Starting {nameof(GetByRuleOneAndTwo)}");

            // Get all flight numbers that match the rule
            var flightNos = await _flightRepository.GetAllFlights()
                .GetDepartbeforeDate(DateTime.Now)
                .GetArrivalBeforeDeparture()
                .Select(s => s.FlightNo).ToArrayAsync();

            if (!flightNos.Any())
                return new List<Flight>();

            // Get all segments for above flight numbers
            var flightSegments = await _flightRepository.GetFlightsByFlightNosAsync(flightNos);

            // convert FlightSegment[] to Flight[]
            return ConvertHelper.ConvertToFlightList(flightSegments.ToList());
        }

        // Rule3
        // Note: It's challenge to implement this rule using sql/ef. Solve it in object collection for now.
        public async Task<IList<Flight>> GetMoreThan2HourGroud()
        {
            _log.LogInformation($"Starting {nameof(GetMoreThan2HourGroud)}");

            // Get all flightSegments
            var flightSegments = await _flightRepository.GetAllFlightsAsync();

            // convert FlightSegment[] to Flight[]
            var flights = ConvertHelper.ConvertToFlightList(flightSegments);

            var matchFlights = new List<Flight>();
            foreach (var flight in flights.Where(f=>f.Segments.Count>1))
            {
                var groudTime = new TimeSpan(0);
                for (int i=0;i< flight.Segments.Count-1;i++)
                {
                    groudTime +=  flight.Segments[i + 1].DepartureDate - flight.Segments[i].ArrivalDate;
                }
                if(groudTime.TotalHours>2)
                    matchFlights.Add(flight);
            }

            return matchFlights;
        }

        // Note: This method is only used to populate demo data to LocalDB. It's not part of this class in real application.
        // Once the demo data are added to DB, this method is not needed any more
        public async Task InitLocalDB()
        {
            var flights = FlightBuilder.GetFlights();
            var flightSegments = ConvertHelper.ConvertToFlightSegmentList(flights);
            await _flightRepository.InsertIntoFlightSegments(flightSegments.ToArray());
        }

    }
}
