﻿using DnataLib.Entity;
using DnataLib.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DnataLib.Repositories
{
    public class FlightRepository: IFlightRepository
    {
        private readonly ILogger<FlightSearchService> _log;
        private readonly DnataDbContext _db;

        public FlightRepository(ILogger<FlightSearchService> log, DnataDbContext db)
        {
            _log = log;
            _db = db;
        }

        // late binding version
        public IQueryable<FlightSegment> GetAllFlights()
        {
            _log.LogDebug("Geting all flights from database...");
            return _db.FlightSegments.OrderBy(f=>f.FlightNo).ThenBy(f=>f.DepartureDate).AsQueryable();
        }

        // early binding version
        public async Task<IList<FlightSegment>> GetAllFlightsAsync()
        {
            _log.LogDebug("Geting all flights from database...");
            return await _db.FlightSegments.OrderBy(f => f.FlightNo).ThenBy(f => f.DepartureDate).ToListAsync();
        }

        public async Task<FlightSegment[]> GetFlightsByFlightNosAsync(string[] flightNos)
        {
            _log.LogDebug("Geting flights by flight numbers from database...");
            var flightNosTemp = flightNos.Select(s => $"'{s}'").ToArray();
            var flightNoCsv = string.Join(",", flightNosTemp); 
            var sql = $"select * from FlightSegments where FlightNo in ({flightNoCsv})";
            return await _db.Set<FlightSegment>()
                .FromSqlRaw(sql).ToArrayAsync();
        }

        // This method is used to populate demo data into database table
        public async Task InsertIntoFlightSegments(FlightSegment[] flightSegments)
        {
            _db.FlightSegments.AddRange(flightSegments);
            await _db.SaveChangesAsync();
        }
    }
}
