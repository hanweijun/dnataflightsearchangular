﻿namespace DnataLib.Model
{
    public class Flight
    {
        public string FlightNo { get; set; }
        public IList<Segment> Segments { get; set; }
    }
}
