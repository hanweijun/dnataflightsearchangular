﻿using Microsoft.EntityFrameworkCore;

namespace DnataLib.Entity
{
    public class DnataDbContext : DbContext
    {
        public DnataDbContext(DbContextOptions<DnataDbContext> options) : base(options)
        {
        }

        public DbSet<FlightSegment> FlightSegments { get; set; }

    }
}
