﻿using System.ComponentModel.DataAnnotations;

namespace DnataLib.Entity
{

    public class FlightSegment
    {
        [Key]
        public string Id { get; set; }
        public string FlightNo { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
    }
}
